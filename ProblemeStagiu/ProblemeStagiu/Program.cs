﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProblemeStagiu {
    public class Program {
        private static readonly string[] A = {"Alice", "Bob", "Jane", "Mary"};

        private static readonly string[] B = {"​John,", "Andrew", "Bob", "Jane"};

        private static readonly FileInfo FileInfo = new FileInfo(
            @"C:\Users\Sabin\source\repos\ProblemeStagiu\ProblemeStagiu\ProblemeStagiu\input.txt");

        public static void Main() {
            DigitSumator();

            ClockOrder();

            ArrayDifference();

            FileEmailSorter(FileInfo);
        }

        private static void DigitSumator() {
            int sum = 0;

            Console.WriteLine("Enter a Number : ");

            int number = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

            while (number != 0) {
                int remainder = number % 10;
                number = number / 10;
                sum = sum + remainder;
            }

            Console.WriteLine("Sum of digits is : " + sum);
            Console.ReadLine();
        }

        private static void ClockOrder() {
            Console.WriteLine("Enter the first time : ");
            string firstTime = Console.ReadLine();
            List<int> firstTimeTokens = firstTime?.Split(':').Select(int.Parse).ToList();

            Console.WriteLine("Enter the second time : ");
            string secondTime = Console.ReadLine();
            List<int> secondTimeTokens = secondTime?.Split(':').Select(int.Parse).ToList();


            if (firstTimeTokens != null) {
                DateTime firstDate = new DateTime() +
                                     new TimeSpan(firstTimeTokens.ElementAt(0), firstTimeTokens.ElementAt(1), 0);

                if (secondTimeTokens != null) {
                    DateTime secondDate = new DateTime() +
                                          new TimeSpan(secondTimeTokens.ElementAt(0), secondTimeTokens.ElementAt(1), 0);

                    if (firstDate < secondDate) {
                        Console.WriteLine($"{firstTime} < {secondTime}");
                    }
                    else {
                        Console.WriteLine(
                            firstDate > secondDate ? $"{firstTime} > {secondTime}" : $"{firstTime} = {secondTime}");
                    }
                }
            }
        }

        private static void ArrayDifference() {
            foreach (string name in A.Where(s1 => B.All(s2 => s1 != s2))) {
                Console.WriteLine(name);
            }
        }

        private static void FileEmailSorter(FileInfo emailFileInfo) {
            List<string> list = new List<string>();
            using (StreamReader reader = emailFileInfo.OpenText()) {
                string line;
                while ((line = reader.ReadLine()) != null) {
                    list.Add(line);
                }
            }

            IList<string> sortedList = list.OrderBy(x => x[0]).ToList();

            foreach (string email in sortedList) {
                Console.WriteLine(email);
            }
        }
    }
}